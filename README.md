# Aadya-Website

Home of the source code for [aadya.tech](https://aadya.tech) built using [Material for MkDocs](https://squidfunk.github.io/mkdocs-material).

## Develop locally:

There are 2 ways to test and develop locally which will serve website accessible under [http://127.0.0.1:8000/](http://127.0.0.1:8000/) which automatically reloads any live changes being made:

1. **Natively**

```bash
$ git clone https://gitlab.com/wisetux/aadya-website.git
$ cd aadya-website
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
$ mkdocs serve
```

2. **Or using Docker**

```bash
$ git clone https://gitlab.com/wisetux/aadya-website.git
$ cd aadya-website
$ docker compose up -d
```

## Build and Deploy:

For production deployment rather than using the development server, we can just output the HTML into a directory and use our own preferred webserver to display the website.

```mkdocs build```

*or*

```docker compose run aadya-website build```

The site will then be output into the `site/` directory. 

> A production grade Nginx server can be deployed at http://localhost:80
> ```docker compose -f docker-compose-prod.yml up -d```

## Screenshots:

### Dark Theme

![Dark Theme](/home/prasad/Workspace/GitLab/aadya-website/screenshots/Screenshot_20250104_030328.png)

### Light Theme

![Light Theme](/home/prasad/Workspace/GitLab/aadya-website/screenshots/Screenshot_20250104_030343.png)
