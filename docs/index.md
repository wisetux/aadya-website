# A small initiative...

Aadya is my journey through technology, and my effort to deploy and provide a set of managed services for my friends and family while documenting the process.

## Guiding Principles

* Self hosted services
* Linux powered Infrastructure
* Open-Source software stack
* Detailed Documentation for anybody interesting in replicating this

> I'm calling them as _Guiding Principles_ and not _Goals_ to facilitiate a more flexible and pragmatic approach.

## Current Deployments

| S. No | Service                                       | Powered By                                                             |
| ----- | --------------------------------------------- | ---------------------------------------------------------------------- |
| 1     | Blog, Website & Wiki                          | [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)    |
| 2     | Cloud storage with collaborative Office Suite | [NextCloud](https://nextcloud.com)                                     |
| 3     | Identity and Access Management (SSO)          | [Keycloak](https://www.keycloak.org/)                                  |
| 4     | Identity Provider (LDAP)                      | [RedHat IdM](https://access.redhat.com/products/identity-management/)  |
| 5     | Password Manager                              | [Vaultwarden](https://www.vaultwarden.net/)                            |
| 6     | Peer to Peer Mesh VPN                         | [Netbird](https://netbird.io/)                                         |

## Future Deployments

| S. No | Service                |
| ----- | ---------------------- |
| 1     | Self Hosted Email      |
| 2     | Photo and Video Backup |
| 3     | Remote Support         |
