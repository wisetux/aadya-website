# Wiki

This Wiki started as a collection of various tips, tricks, and tutorials I’ve learned while dealing with tech on a day to day basis. The primary focus is going to be around Linux and OpenSource, but other desktop and mobile OS are also covered. Whether you’re a beginner or an experienced user, you’ll find valuable information here to help you learn, deploy and troubleshoot your environment.

## How to Contribute

If you want to help contribute information to this resource pool, then you’ve come to the right place. Contributions are welcome and encouraged! You can contribute by visiting [GitLab Repository](https://gitlab.com/wisetux/aadya-website) and submitting your tips, tricks, or tutorials.

You have just taken your first step toward getting involved. Before you get started, request you to please observe the [Code of Conduct](../code-of-conduct.md). It’s not very long and it will help you get started.

## Topics Covered

Here is everything that is included:

1. __Android__: Tips and tricks for optimizing your Android device, troubleshooting common issues, and making the most out of your apps.
2. [__Linux__](linux/index.md): Tutorials and guides for various Linux distributions, command-line tips, and system administration tricks.
3. [__VMware__](vmware/index.md): Best practices for using VMware, including setup guides, performance optimization, and troubleshooting.
