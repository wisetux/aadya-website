---
title: Perpetually New Blog
authors:
  - wisetux
date:
  created: 2024-09-15T10:09:20
  updated: 2024-12-31T10:09:20
tags:
  - Blogging
  - Hugo
  - MkDocs
---

I was thinking to start building this website without any kind of a welcome post. Coz at this point I have lost count of the number of times I have tried to create a blog that I was happy with keeping around. My OCD for quality writing didn’t help either.
<!-- more -->
I had a on and off journey with blogging which started with Blogger way back in 2011. I called my Blog as "[Centurian](https://www.centurian.blogspot.com)" since I was this stupid young self who somehow thought this blog would still be up for a century. Well, that didn't last a year.

In 2012, I got my first server — a Raspberry Pi Model B using which I started gaining experience with headless Linux machines. However, during this time Facebook and Google+ were where I would post until I quit social media. And then in 2015 I registered __aadya.tk__ for free on *Freenom* and began self hosting my very first WordPress installation.

In 2017, I bought the current domain __aadya.tech__ as a birthday gift for myself. Initially it hosted my NextCloud instance, which later became my WordPress blog. It was around the time Spectre and Meltdown vulnerabilities were discovered and I wanted to write about how to verify if your system was patched. An old broken laptop which I was handed down became my server during this period. Once again, the blog fizzled out, and I noticed a pattern.

In 2022, I realized I needed to simplify my blogging platform due to my ever-changing server environment. I decided to drop WordPress and its database in favor of [GravCMS](https://getgrav.org/) — a flat-file CMS. All posts were written as flat files on the disk, eliminating the need for a database. My blog felt more responsive with Grav than with WordPress.

Now, my site has taken its current form. I’ve switched to Hugo, an extremely fast static site generator. I’m now comfortable writing in Markdown, and with automated deployments, my entire site lives on Git.

So this is my perpetually new blog. I’m excited to continue sharing my thoughts and experiences here, confident that this iteration will stand the test of time. Thank you for joining me on this journey, and I look forward to what the future holds.
