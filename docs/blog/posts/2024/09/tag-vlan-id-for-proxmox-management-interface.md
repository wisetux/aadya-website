---
title: Tag VLAN ID for Proxmox Management Interface
authors:
  - wisetux
date: 
  created: 2024-09-26T10:09:20
tags:
  - Proxmox
  - Linux Networking
  - VLANs
  - Bug
---

For a project I’m working on, I needed to set up a VLAN-tagged Proxmox management interface using Linux Bridges. I had previously used a similar setup with Open vSwitch and assumed it would be the same with Linux Bridges. However, I [encountered issues](#ip-assigned-to-vlan-interface-sdn) when trying to assign an IP from the management VLAN to the Proxmox host and create a management VLAN for VMs using SDN.
<!-- more -->
## IP Assigned to Linux Bridge

In a default Proxmox installation, the IP address provided during the hypervisor setup is assigned to a “Linux Bridge” interface linked to the physical port. Assuming there are no VLANs involved and we are working with a flat network for the management interface, the configuration looks straightforward.
![Default Linux Bridge with IP Address](../../../../assets/images/blog/Screenshot_20240926_100303.png "_Default Linux Bridge with IP Address_")

??? note "`/etc/network/interfaces`" 
    
    ```
    auto lo
    iface lo inet loopback

    iface enp129s0f0np0 inet manual

    iface enp129s0f1np1 inet manual

    auto eno1
    iface eno1 inet manual

    iface eno2 inet manual

    iface eno3 inet manual

    iface eno4 inet manual

    auto vmbr0
    iface vmbr0 inet manual
        address 10.12.1.11/24
        gateway 10.12.1.1
        bridge-ports eno1
        bridge-stp off
        bridge-fd 0

    source /etc/network/interfaces.d/*
    ```

## IP Assigned to VLAN Interface

Now, let’s consider that the management network belongs to a VLAN, and we need to assign an IP from that VLAN to Proxmox to access the web UI. So in the previous configuration, mark the bridge as VLAN aware and then add a "Linux VLAN" interface.

I named this interface "pve" to assign an IP from VLAN 99 to our Proxmox host.
![Creating new VLAN Interface](../../../../assets/images/blog/Screenshot_20240926_123600.png "_Creating new VLAN Interface_")
![IP Assigned to VLAN Interface](../../../../assets/images/blog/Screenshot_20240926_123622.png "_IP Assigned to VLAN Interface_")

??? note "`/etc/network/interfaces`" 
    
    ```
    auto lo
    iface lo inet loopback

    iface enp129s0f0np0 inet manual

    iface enp129s0f1np1 inet manual

    auto eno1
    iface eno1 inet manual

    iface eno2 inet manual

    iface eno3 inet manual

    iface eno4 inet manual

    auto vmbr0
    iface vmbr0 inet manual
        bridge-ports eno1
        bridge-stp off
        bridge-fd 0
        bridge-vlan-aware yes
        bridge-vids 2-4094

    auto pve
    iface pve inet static
        address 10.12.99.12/24
        gateway 10.12.99.1
        vlan-id 99
        vlan-raw-device vmbr0
    #Proxmox Management

    source /etc/network/interfaces.d/*
    ```

## IP Assigned to VLAN Interface + SDN

The problem arose when I tried to assign an IP from the management VLAN to the Proxmox host while also creating a management VLAN for VMs using SDN. With the above setup, I created a SDN VNet called "mgmt".
![SDN Management VNet](../../../../assets/images/blog/Screenshot_20240926_131815.png "_SDN Management VNet_")
The goal was to be able to assign IP from management VLANs to Virtual Machines. But instead, I lost access to the Proxmox node. I had to drop into Linux terminal and try to manually restart networking service which created all kinds of weird behavior ranging from the host management IP being unavailable, to the SDN interface not providing management network to VMs.

After a lot of headscratching and going through Proxmox documents and forums, I came across this [bug report](https://bugzilla.proxmox.com/show_bug.cgi?id=5474).

Essentially, adding a VNet from the GUI creates following configuration in `/etc/network/interfaces.d/sdn`

```
auto mgmt
iface mgmt
    bridge_ports vmbr0.99
    bridge_stp off
    bridge_fd 0
    alias Management Network
```

This conflicts with our previously created Linux VLAN. The solution for now is to use the same name for SDN VNet and Linux VLAN.

### Potential Workaround

This cannot be done via the GUI and needs to be done by manually editing `/etc/network/interfaces`. Change **pve** (Linux VLAN interface name) to **mgmt** (SDN VNet interface name) and remove details of the parent interface as this would be referenced from the SDN configuration. Thus my final configuration looks like:

??? note "`/etc/network/interfaces`" 
    
    ```
    auto lo
    iface lo inet loopback

    iface enp129s0f0np0 inet manual

    iface enp129s0f1np1 inet manual

    auto eno1
    iface eno1 inet manual

    iface eno2 inet manual

    iface eno3 inet manual

    iface eno4 inet manual

    auto vmbr0
    iface vmbr0 inet manual
        bridge-ports eno1
        bridge-stp off
        bridge-fd 0
        bridge-vlan-aware yes
        bridge-vids 2-4094

    auto mgmt
    iface mgmt inet static
        address 10.12.99.12/24
        gateway 10.12.99.1
    #Proxmox Management

    source /etc/network/interfaces.d/*
    ```

??? note "`/etc/network/interfaces.d/sdn`" 

    ```
    auto mgmt
    iface mgmt
        bridge_ports vmbr0.99
        bridge_stp off
        bridge_fd 0
        alias Management Network

    auto nihar
    iface nihar
        bridge_ports vmbr0.30
        bridge_stp off
        bridge_fd 0
        alias Nihar Lab

    auto pkt0
    iface pkt0
        bridge_ports vmbr0.50
        bridge_stp off
        bridge_fd 0
        alias PKT0

    auto pkt1
    iface pkt1
        bridge_ports vmbr0.51
        bridge_stp off
        bridge_fd 0
        alias PKT1

    auto sweha
    iface sweha
        bridge_ports vmbr0.52
        bridge_stp off
        bridge_fd 0
        alias SWE HA
    ```

The only side effect I noticed so far with this workaround is that the VLAN interface in Proxmox GUI shows up as _Unknown_.

![Linux VLAN with SDN VNet](../../../../assets/images/blog/Screenshot_20240926_141814.png "_Linux VLAN with SDN VNet_")
