---
title: Aadya Wiki
authors:
  - wisetux
date: 
  created: 2024-12-29T07:00:06
tags:
  - MkDocs
---

I've been wanting to work on my personal wiki for a long time. It was meant to be a knowledge hub, combining all my notes and text files in one organized place. However, much like my website, it turned into it's own journey.
<!-- more -->
There are many fantastic options for a wiki, but none lasted long for me. The earliest one I had was deployed using [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) which powers the fantastic Wikipedia website. However, I needed something less complicated, which was when I stumbled upon BookStack while toying with DietPi a couple of years ago.

[BookStack](https://www.bookstackapp.com/) was simple to use with a nice UI. I started documenting some of my work back then but soon realized that its rigid Shelf/Book/Chapter/Page structure wasn't suitable for me. Thus, my search continued.

Then, I found [Wiki.js](https://js.wiki/) which was perfect. It has a nice UI, was flexible and was actively being developed. I donated to the project and also deployed not only for myself, but also into production for our company's internal wiki when we were searching for one. It has been running extremely well for over 4 years now. It supports writing in Markdown for technical writers while also offering a WYSIWYG editor for everyone else. Combined this with the upcoming v3 ([vega](https://github.com/requarks/wiki/tree/vega)) shows a lot of potential for Wiki.js. But I was looking for something simpler that utilized flat files, making backups easier and the entire setup more reproducible.

So for last few days I have been testing [DokuWiki](https://www.dokuwiki.org/). It was close to what I wanted as it required no database and all pages were just plain .txt files inside folders. The theming made the wiki look both clean and modern. However, there was a few issues I got with DokuWiki.

- Lack of native tagging capabilities
- Weird URL scheme consisting of colons, like https://wiki.aadya.tech/doku.php/linux:ubuntu, which wasn't improved even with URL rewrite
- DokuWiki has it's own markup syntax which isn't compatible with Markdown.

While I tried Gollum, I finally settled on yet another Static Site Generator [MkDocs](https://www.mkdocs.org/); more precisely [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/). It uses Markdown, has an adaptive modern UI with no database dependency, is customizable, and can seamlessly integrate with version control allowing CI/CD. Haven't written anything on this new Wiki yet, other than some boilerplate. I'm yet to create any proper CI/CD around this but still I believe I think this time [Aadya Wiki](https://wiki.aadya.tech) is going live. I look forward to moving all my txt files and saved articles to this Wiki.
